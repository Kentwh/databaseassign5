<?php

/** Club class: 
 * @author Kent Holt
 * 
 * Attributes:
 * @param integer $id Club id
 * @param string $name Club name
 * @param string $city Club city locaiton
 * @param string $county Club county locaiton
 */
class Club {
	public $id;
	public $name;
	public $city;
	public $county;

	/** Constructor
	 * @param string $id Club id
	 * @param string $name Club name
	 * @param string $city Club city locaiton
	 * @param string $county Club county locaiton
	 */
	public function __construct($id, $name, $city, $county) 
    {  
        $this->id = $id;
        $this->name = $name;
	    $this->city = $city;
	    $this->county = $county;
    } 
}

?>