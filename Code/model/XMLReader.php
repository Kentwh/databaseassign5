<?php
include_once("Skier.php");
include_once("Club.php");

/** The Model is the class hvolding data about a collection of this->skiers and this->clubs.
 */
class _XMLReader
{
    protected $doc;
    protected $x;

    public $skiers;
    public $clubs;
    public $reps;

    public function __construct()
    {
        //
    }

    public function readXMLFile()
    {
        $this->doc = new DOMDocument;
        $this->doc->load("./xml/SkierLogs.xml");

        $this->x = new DOMXPath($this->doc);

        $this->skiers = Array();
        $this->clubs = Array();
        $this->reps = Array(Array());

        // Get all skiers and save them
        $i = 0;
        foreach ($this->x->query("/SkierLogs/Skiers/Skier") as $elem)
        {
            $this->skiers[$i] = new Skier($elem->getAttribute("userName"), 
                                          $elem->childNodes->item(1)->textContent, 
                                          $elem->childNodes->item(3)->textContent, 
                                          $elem->childNodes->item(5)->textContent);
            /*$username = $elem->getAttribute("userName");
            $fname = $elem->childNodes->item(1)->textContent;
            $lname = $elem->childNodes->item(3)->textContent;
            $year_of_birth = $elem->childNodes->item(5)->textContent;*/

            /*printf("UName: %s\nFName: %s\nLName: %s\nYOB: %d\n\n", 
                $this->skiers[$i]->username, 
                $this->skiers[$i]->fname, 
                $this->skiers[$i]->lname, 
                $this->skiers[$i]->year_of_birth);
            $i++;*/
        }

        // Get all clubs and save them
        $i = 0;
        foreach ($this->x->query("/SkierLogs/Clubs/Club") as $elem)
        {
            $this->clubs[$i] = new Club($elem->getAttribute("id"), 
                                        $elem->childNodes->item(1)->textContent, 
                                        $elem->childNodes->item(3)->textContent, 
                                        $elem->childNodes->item(5)->textContent);

            /*printf("ID: %s\nName: %s\nCity: %s\nCounty: %d\n\n", 
                $this->clubs[$i]->id, 
                $this->clubs[$i]->name, 
                $this->clubs[$i]->city, 
                $this->clubs[$i]->county);*/
            $i++;
        }

        // Get all logs and save them
        /*foreach ($this->x->query("/SkierLogs/Season") as $seas)
        {
            $i = 1;
            foreach ($this->x->query("/Skiers", $seas) as $skiers)
            {
                foreach($this->x->query("/Skier", $skiers) as $skier)
                {
                    $this->reps[$i]["season"] = $seas->getAttribute("fallYear")->textContent;
                    $this->reps[$i]["club"] = $skiers->getAttribute("clubId")->textContent;
                    $this->reps[$i]["skier"] = $skier->item($i)->getAttribute("userName")->textContent;
                    $this->reps[$i]["dist"] = $this->x->query(
                        "sum(/Log/Entry/Distance)", $skier)->item(0)->textContent;

                    printf("SEASON: %s | CLUB: %s | SKIER: %s | DIST: %d | ", 
                        $this->reps[$i]["season"], 
                        $this->reps[$i]["club"], 
                        $this->reps[$i]["skier"], 
                        $this->reps[$i]["dist"]);
    
                    $i+=2;
                }
            }

            //sum(/SkierLogs/Season[@fallYear="2015"]/Skiers[@clubId="asker-ski"]/Skier[@userName="andr_stee"]/Log/Entry/Distance)
        }*/
    }

}

?>
