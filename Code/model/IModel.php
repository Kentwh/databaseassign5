<?php
Interface IModel {
    /** Function returning the complete list of skiers in the collection. Skiers are
     * returned in order of username.
     * @return Skiers[] An array of skier objects indexed and ordered by their username.
	 * @throws PDOException
     */
    public function getSkierList();

    /** Function retrieving information about a given skier in the collection.
     * @param string $username the username of the skier to be retrieved
     * @return Skier|null The skier matching the $username exists in the collection; null otherwise.
	 * @throws PDOException
     */
    /*public function getSkierByUsername($username);*/

    /** Adds a new skier to the collection.
     * @param $skier Skier The skier to be added - the username of the skier must be set before insertion
	 * @throws PDOException
     */
    public function addSkier($skier);

    /** Function returning the complete list of clubs in the collection. Clubs are
     * returned in order of id.
     * @return Clubs[] An array of club objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getClubList();

    /** Function retrieving information about a given club in the collection.
     * @param string $id the id of the club to be retrieved
     * @return Skier|null The club matching the $is exists in the collection; null otherwise.
	 * @throws PDOException
     */
    /*public function getClubByID($id);*/

    /** Adds a new club to the collection.
     * @param $club Club The club to be added - the id of the club will be set after successful insertion.
	 * @throws PDOException
     */
    public function addClub($club);
}
?>