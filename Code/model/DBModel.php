<?php
include_once("IModel.php");
include_once("Skier.php");
include_once("Club.php");

/** The Model is the class holding data about a collection of skiers and clubs.
 */
class DBModel implements IModel
{
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;

    /**
	 * @throws PDOException
     */
    public function __construct($db = null)
    {
	    if ($db)
		{
			$this->db = $db;
		}
		else
		{
            $this->db = new PDO('mysql:host=localhost;dbname=Assign5;charset=utf8mb4', 'root', '');
		}
    }

    public function addXMLDataToDB($skiers, $clubs, $reps)
    {
        //
    }

    /** Function returning the complete list of skiers in the collection. Skiers are
     * returned in order of username.
     * @return Skiers[] An array of skier objects indexed and ordered by their username.
	 * @throws PDOException
     */
    public function getSkierList()
    {
        $skierlist = Array();
        $sqlGetSkiers = $this->db->query("SELECT * FROM `skier` ORDER BY username ASC"); // Setup a query to get all skiers, order by username
        while($row = $sqlGetSkiers->fetch(PDO::FETCH_ASSOC))                     // Loop while you get something from fetch
        {
            $skierlist[] = new Skier($row['username'], $row['fname'], $row['lname'], $row['year_of_birth']); // Save new skier to list
        }
        return $skierlist;
    }

    /** Function retrieving information about a given skier in the collection.
     * @param string $username the username of the skier to be retrieved
     * @return Skier|null The skier matching the $username exists in the collection; null otherwise.
	 * @throws PDOException
     */
    /*public function getSkierByUsername($username)
    {
        $book = NULL;
        // Check if $id constains only numeric values, if not, return saying something went wrong
        if (!is_numeric($id) || $id <= 0) { return null; }
        // Prepare statment to get all book with given id
        $sqlGetBookById = $this->db->prepare("SELECT * FROM Books WHERE id=:ID");
        $sqlGetBookById->bindValue(':ID', $id, PDO::PARAM_INT);                 // Bind id

        $sqlGetBookById->execute();                                             // Execute sql statement

		$fetchedBook = $sqlGetBookById->fetch(PDO::FETCH_ASSOC);                // Fetch first book in the result
        // Transfer the results into a new book
        $book = new Book($fetchedBook['title'], $fetchedBook['author'], $fetchedBook['description'], $fetchedBook['id']);
        // Test to see if result is a valid result, if not, throw Exception and return NULL
        if ($book->title == NULL || $book->author == NULL || $book->id == NULL) { throw new Exception(); return null; }
        return $book;
    }*/

    /** Adds a new skier to the collection.
     * @param $skier Skier - The skier to be added - the username of the skier must be set before insertion
	 * @throws PDOException
     * @return true|false - true if added, false if failed
     */
    public function addSkier($skier)
    {
        // Is any attribute empty / out of scope, abort!
        if ($skier->username === "" || 
            $skier->fname === "" || 
            $skier->lname === "" ||
            $skier->year_of_birth > 0 ||
            $skier->year_of_birth <= 9999) { throw new Exception(); return false; }

        // Prepare statement to insert a skier
        $sqlAddSkier = $this->db->prepare("INSERT INTO skier (username, fname, lname, year_of_birth) VALUES (:username, :fname, :lname, :year_of_birth)");
        $sqlAddSkier->bindValue(":username", $skier->username, PDO::PARAM_STR);              // Bind username
        $sqlAddSkier->bindValue(":fname", $skier->fname, PDO::PARAM_STR);                    // Bind fname
        $sqlAddSkier->bindValue(":lname", $skier->lname, PDO::PARAM_STR);                    // Bind lname
        $sqlAddSkier->bindValue(":year_of_birth", $skier->year_of_birth, PDO::PARAM_STR);    // Bind year_of_brith
        $sqlAddSkier->execute();                                                             // Execute sql statement
        if ($this->db->lastInsertId() !== $skier->username) return true;                    // Validate successful addition
        return false;                                                                       // Addition failed!
    }

    /** Function returning the complete list of clubs in the collection. Clubs are
     * returned in order of id.
     * @return Clubs[] An array of club objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getClubList()
    {
        $clublist = Array();
        $sqlGetClubs = $this->db->query("SELECT * FROM `club` ORDER BY id ASC"); // Setup a query to get all clubs, order by id
        while($row = $sqlGetClubs->fetch(PDO::FETCH_ASSOC))                     // Loop while you get something from fetch
        {
            $clublist[] = new Club($row['id'], $row['name'], $row['city'], $row['county']); // Save new club to list
        }
        return $clublist;
    }

    /** Function retrieving information about a given club in the collection.
     * @param string $id the id of the club to be retrieved
     * @return Skier|null The club matching the $is exists in the collection; null otherwise.
	 * @throws PDOException
     */
    /*public function getClubByID($id)
    {
        //
    }*/

    /** Adds a new club to the collection.
     * @param $club Club - The club to be added - the id of the club will be set after successful insertion.
	 * @throws PDOException
     * @return true|false - true if added, false if failed
     */
    public function addClub($club)
    {
        // Is any attribute empty / out of scope, abort!
        if ($club->name === "" || 
            $club->city === "" ||
            $club->county === "") { throw new Exception(); return false; }

        // Prepare statement to insert a club
        $sqlAddClub = $this->db->prepare("INSERT INTO club (name, city, county) VALUES (:name, :city, :county)");
        $sqlAddClub->bindValue(":name", $club->name, PDO::PARAM_STR);           // Bind name
        $sqlAddClub->bindValue(":city", $club->city, PDO::PARAM_STR);           // Bind city
        $sqlAddClub->bindValue(":county", $club->county, PDO::PARAM_STR);       // Bind year_of_brith
        $sqlAddClub->execute();                                                 // Execute sql statement
        $club->id = $this->db->lastInsertId();                                  // Get last inserted id from DB
        return true;
    }
}

?>
