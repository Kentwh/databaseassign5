<?php

/** Skier class:
 * @author Kent Holt
 * 
 * Attributes:
 * @param string $username Skier username
 * @param string $fname Skier first name
 * @param string $lname Skier last name
 * @param integer $year_of_birth Skier year of birth
 */
class Skier {
	public $username;
	public $fname;
	public $lname;
	public $year_of_birth;

	public $rep; 

	/** Constructor
	 * @param string $username Skier username
	 * @param string $fname Skier first name
	 * @param string $lname Skier last name
	 * @param integer $year_of_birth Skier year of birth
	 */
	public function __construct($username, $fname, $lname, $year_of_birth)  
    {  
        $this->username = $username;
        $this->fname = $fname;
	    $this->lname = $lname;
		$this->year_of_birth = $year_of_birth;
		
		$this->rep = Array(Array());
    } 
}

?>