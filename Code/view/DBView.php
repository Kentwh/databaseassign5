
<?php

include_once('View.php');

/** The BookView is the class that creates the page showing details about one book. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
Class DBView extends View {
	protected $skiers;
	protected $clubs;
	protected $opParamName;
	protected $delOpName;
	
    /** Constructor 
     * @author Rune Hjelsvold
	 * @param Skiers $s
	 * @param Book $book The book to be shown.
	 * @param string $opParamName The name of the parameter to used in the query string for passing the operation to be performed.
	 * @param string $delOpName The name to be used for the delete operation.
	 * @param string $modOpName The name to be used the modify operation.
     * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
     */
	
	
 /*
	
	public function __construct($skiers, $clubs)  
    {  
        $this->book = $book;
        $this->opParamName = $opParamName;
    } 
	
	/** Used by the superclass to generate page title
	  * @return string Page title.
	  *
	protected function getPageTitle() {
		return 'Default View';
	}
	
	/** Helper function generating HTML code for creating an list entry of skiers
	 *
	protected function createSkierView() {
		return 
		'<div id="delForm">'
		. '<h6>Skier'$this->book->id.'" type="hidden" />';
	}
	
	/** Helper function generating HTML code for the form for modifying book data
	 *
	protected function createModifyForm() {
		return 
		'<form id="modForm" action="index.php" method="post">'
		. '<input name="'.$this->opParamName.'" value="'.$this->modOpName.'" type="hidden" />'
		. '<input name="id" value="'.$this->book->id.'" type="hidden"/>'
		. 'Title:<br/>'
		. '<input name="title" type="text" value="'.htmlspecialchars($this->book->title).'" /><br/>'
		. 'Author:<br/>'
		. '<input name="author" type="text" value="'.htmlspecialchars($this->book->author).'" /><br/>'
		. 'Description:<br/>'
		. '<input name="description" type="text" value="'.htmlspecialchars($this->book->description).'" /><br/>'
        . '<input type="submit" value="Update book record" />'
        . '</form>';
	}
	
	/** Used by the superclass to generate page content
	 *
	protected function getPageContent() {
        return 'ID:' . $this->book->id
			   . $this->createModifyForm()
			   . $this->createDeleteButton()
			   . '<p><a href=index.php>Back to book list</a></p>';
	}
	*/
}
?>