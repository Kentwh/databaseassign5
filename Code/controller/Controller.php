<?php
include_once("model/DBModel.php");
include_once("model/XMLReader.php");
include_once("view/ErrorView.php");
//include_once("view/DBView.php");

/** The Controller is responsible for instructing XMLReader and DB
 */
class Controller {
	public $model;
	public $xread;

	public function __construct()
    {
		session_start();

		$this->model = new DBModel();
		$this->xread = new _XMLReader();
    }

/** The one function running the controller code.
 */
	public function invoke()
	{
		$this->xread->readXMLFile();
		//$model->addXMLDataToDB($xpath->skiers, $xpath->clubs, $xpath->reps);
	}
}

?>
