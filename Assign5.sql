-- phpMyAdmin SQL Dump
-- version 4.7.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 05, 2017 at 11:00 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Assign5`
--

-- --------------------------------------------------------

--
-- Table structure for table `club`
--

CREATE TABLE `club` (
  `id` varchar(50) COLLATE utf8_danish_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_danish_ci NOT NULL,
  `city` varchar(50) COLLATE utf8_danish_ci NOT NULL,
  `county` varchar(50) COLLATE utf8_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `represents`
--

CREATE TABLE `represents` (
  `uname` varchar(50) COLLATE utf8_danish_ci NOT NULL,
  `cid` varchar(50) COLLATE utf8_danish_ci NOT NULL,
  `season` int(4) NOT NULL,
  `dist` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `skier`
--

CREATE TABLE `skier` (
  `username` varchar(50) COLLATE utf8_danish_ci NOT NULL,
  `fname` varchar(50) COLLATE utf8_danish_ci NOT NULL,
  `lname` varchar(50) COLLATE utf8_danish_ci NOT NULL,
  `year_of_birth` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `club`
--
ALTER TABLE `club`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `represents`
--
ALTER TABLE `represents`
  ADD PRIMARY KEY (`uname`,`cid`),
  ADD KEY `represents_FK_club` (`cid`);

--
-- Indexes for table `skier`
--
ALTER TABLE `skier`
  ADD PRIMARY KEY (`username`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `represents`
--
ALTER TABLE `represents`
  ADD CONSTRAINT `represents_FK_club` FOREIGN KEY (`cid`) REFERENCES `club` (`id`),
  ADD CONSTRAINT `represents_FK_skier` FOREIGN KEY (`uname`) REFERENCES `skier` (`username`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
